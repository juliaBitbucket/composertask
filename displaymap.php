<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Карта</title>
    <style  type="text/css">
        #map {
            width: auto;
            height: 600px;
        }
    </style>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>
<div class="container">
    <a href="search.php">Вернуться назад</a>
    <div class="row">
        <div id="map" class="col-md-12"></div>
    </div>
</div>
</body>
</html>

<script type="text/javascript">
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init() {
        myMap = new ymaps.Map("map", {center: [<?php echo $_GET['lat']?>, <?php echo $_GET['lon']?>], zoom: 14});

        myPlacemark = new ymaps.Placemark([<?php echo $_GET['lat']?>, <?php echo $_GET['lon']?>], {hintContent: 'Mark'});

        myMap.geoObjects.add(myPlacemark);
    }
</script>