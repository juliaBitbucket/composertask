<?php
require_once 'map.php';

error_reporting( E_ERROR );

$addresses = [];
if (!empty($_POST['address'])) {
    $map = new Map();
    $addresses = $map->getCoordinateAddress($_POST['address']);}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Адресса</title>
    <style  type="text/css">
        .form-signin {
            max-width: 400px;
            min-height: 190px;
            padding: 19px 29px 29px;
            margin: 60px auto auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin input, .form-signin button {
            margin-bottom: 10px;
        }
    </style>
    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <form class="form-signin" role="form" method="post">
                <textarea class="form-control" name="address" rows="4" placeholder="Введите адресс" required autofocus></textarea><br>
                <button class="btn btn-lg btn-primary" name="action" type="submit">Найти</button>
            </form>
        </div>
        <div class="col-md-8">
            <h1>Список адрессов</h1>
            <hr>
            <ol>
                <?php if (!empty($addresses)):
                    foreach ($addresses as $address): ?>
                        <li><?php echo $address['address']; ?> -
                            <a href="displaymap.php?lat=<?php echo $address['latitude']; ?>&lon=<?php echo $address['longitude']; ?>">
                                <?php echo 'Широта -' . $address['latitude'] . ' Долгота' . $address['longitude']; ?></a></li>
                    <?php endforeach; endif; ?>
            </ol>
            <hr>
        </div>
    </div>
</div>
</body>
</html>
