<?php
class Map
{
    public function __construct()
    {
        require_once 'vendor/autoload.php';
    }

    public function getCoordinateAddress($address)
    {
        $api = new \Yandex\Geo\Api();

        $api->setQuery($address);
        $api->setLimit(10)
            ->setLang(\Yandex\Geo\Api::LANG_US)
            ->load();

        $response = $api->getResponse();
        $listAddresses = $response->getList();

        $address = [];
        foreach ($listAddresses as $item) {
            $address[] = [
                'address' => $item->getAddress(),
                'latitude' => $item->getLatitude(),
                'longitude' => $item->getLongitude()
            ];
        }
        return $address;
    }
}